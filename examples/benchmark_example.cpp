#include <algorithm>
#include <cassert>
#include <ctime>
#include <iostream>
#include <string>
#include <vector>

#include "counter.h"
#include "radix.h"

bool CheckOrder(const std::vector<int>& data) {
    for (size_t idx = 1; idx < data.size(); ++idx) {
        if (data[idx - 1] > data[idx]) {
            return false;
        }
    }
    return true;
}

void StdSort(std::vector<int>& data) {
    std::sort(data.begin(), data.end());
}

void RunSort(std::vector<int>& data, std::string type) {
    time_t start = time(0);
    if (type == "radix") {
        RadixSort(data); // from radix.h
    } else if (type == "counter") {
        CounterSort(data); // from counter.h
    } else if (type == "std") {
        StdSort(data);
    } else {
        std::cerr << "Unknown type of sorting" << std::endl;
        return;
    }
    time_t end = time(0);
    assert(CheckOrder(data) == true);
    std::cout << "Time of working " << type << ": " << end - start << std::endl;
}

int main(int argc, const char* argv[]) {
    if (argc != 4) {
        std::cerr << "Usage: ./benchmark count sort1 sort2" << std::endl;
        return 0;
    }

    size_t size = atoi(argv[1]);
    std::vector<int> data;
    data.resize(size);
    const int maxNum = 1000000;
    for (size_t idx = 0; idx < size; ++idx) {
        data[idx] = rand() % maxNum;
    }
    std::vector<int> dataCopy(data);

    RunSort(data, std::string(argv[2]));
    RunSort(dataCopy, std::string(argv[3]));

    return 0;
}
